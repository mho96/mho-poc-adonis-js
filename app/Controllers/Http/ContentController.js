'use strict'

const Redis = use('Redis')
const uniqid = require('uniqid')

class ContentController {
    
    /**
     * Display a listing of the resource.
     */
    async index() {
        const res = await Redis.hgetall('contents')
        const list = Object.values(res).map(val => JSON.parse(val))
        return list
    }

    
    /**
     * Store a newly created resource in storage.
     */
    async store({ request, response }) {
        const fields = request.only(['title', 'body'])
        const id = uniqid()
        const created = Math.floor(Date.now()/1000)
        const content = { id: id , created: created, ...fields }
        await Redis.hset('contents', id, JSON.stringify(content))
        response.status(201).send({ id })

    }

    /**
     * Display the specified resource.
     */
    async show({ params, response }) {
        try {
            const content = await Redis.hget('contents', params.id)
            if (content === null) throw new Error('not found')
            return JSON.parse(content)
        } catch (Error) {
            response.status(404).send(`${params.id} does not exist`)
        }
    }

    /**
     * Update the specified resource in storage.
     */
    async update({ request, params }) {
        const fields = request.only(['title', 'body'])
        const created = Math.floor(Date.now()/1000)
        const content = { id: params.id , created: created, ...fields }
        await Redis.hset('contents', params.id, JSON.stringify(content))
        return this.show(request)
    }
    
    /**
     * Remove the specified resource from storage.
     */
    async destroy({ params }) {
        const content = await Redis.hdel('contents', params.id)
        const message = content ? `content ${params.id} deleted` : `something went wrond when deleting content ${params.id}`
        return { message }
    }
    
}

module.exports = ContentController
